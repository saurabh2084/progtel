from flask import Flask, render_template, request, session, redirect, make_response, send_file
from flaskext.mysql import MySQL
from datetime import datetime
import os
from flask import send_from_directory
from flask_weasyprint import HTML, render_pdf
import numpy
import ujson
import json
from collections import OrderedDict
import pdfkit
from xhtml2pdf import pisa
from StringIO import StringIO
import itertools
from pprint import pprint

# import logging
# logging.basicConfig()
import logging, sys
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# from OpenSSL import SSL



# from werkzeug import generate_password_hash, check_password_hash
syntaxFile = "../Config/Syntactic_Mapping.json"
semanticFile = "../Config/semantic_Mapping.json"
logserverFile = "../Config/logserver_Mapping.json"
db_column_name=0
column_order=1
datatype_index=2
summary_index=3
app = Flask(__name__)


app.secret_key = 'my secret key'
# session.clear()
mysql = MySQL()

serveraddr = "localhost"
dbuser = "root"
dbpass = "123456"
dbname = "PROGTELDB"
tablename = "IPS_Data"
viewname = "IPS_Data_view"
summaryViewName = "summary_view"
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = dbuser
app.config['MYSQL_DATABASE_PASSWORD'] = dbpass
app.config['MYSQL_DATABASE_DB'] = dbname
app.config['MYSQL_DATABASE_HOST'] = serveraddr
mysql.init_app(app)
# print (mysql.__version__)


# _hashed_password = generate_password_hash(dbpass)
# print(_hashed_password)

tableorview = [tablename]

print "Server started at " + str((datetime.now()))

# TODO: Protect queries from sql injection



@app.route("/")
def main():
    return render_template('index.html')

currFilterMap = {}
currSortMap = {}
dynamicFilterMap = {}


@app.route('/showDB', methods=['GET', 'POST'])
def showDB():
    if session.get('user'):

        with open(syntaxFile) as syntax_file:
            syntaxMap = ujson.load(syntax_file)

        with open(semanticFile) as semantic_file:
            semanticMap = ujson.load(semantic_file)
        dateCol = syntaxMap[semanticMap["Latest Timestamp"]][db_column_name]
        countCol = ""
        if "Count" in semanticMap:
            countCol = syntaxMap[semanticMap["Count"]][db_column_name]
        # srcipColumn = syntaxMap[semanticMap["Source IP"]][db_column_name]
        # destipColumn = syntaxMap[semanticMap["Destination IP"]][db_column_name]
        conn = mysql.connect()
        globcursor = conn.cursor()
        globcursor.execute("SELECT column_name FROM information_schema.columns WHERE table_name='" + tablename + "'")
        column = globcursor.fetchall()
        minmaxdatequery = ("Select min(`{0}`), max(`{0}`) from " + tablename + ";").format(dateCol)
        globcursor.execute(minmaxdatequery)
        minmax = globcursor.fetchone()
        minMaxDateList = []
        minMaxCountList = []
        for dateobj in minmax:
            minMaxDateList.append(dateobj.strftime('%Y-%m-%d'))
        if countCol:
            minmaxcountquery = ("Select min(`{0}`), max(`{0}`) from " + tablename + ";").format(countCol)
            globcursor.execute(minmaxcountquery)
            minmax = globcursor.fetchone()
            for countobj in minmax:
                minMaxCountList.append(countobj)
        print "minMaxCountList: ", minMaxCountList

        columnMap = {}
        # print "datecol is ", dateCol
        # print "countcol is ", countCol
        for columns in column:
            if "{0}".format(columns[0]) not in [dateCol, countCol]:
                # queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tablename)
                queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tableorview[0])
                globcursor.execute(queryString)
                columnMap[columns[0]] = globcursor.fetchall()
        conn.close()
        if request.method == 'POST':
            print (request.form)
            if request.form['submitbtn'] == 'SORT':
                sortCol = request.form['sortColumn']
                sortDir = request.form['sortDir']
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute("Select * from " + tableorview[0] + " order by `" + sortCol + "` " + sortDir)
                data = cursor.fetchall()
                # print (data)
                conn.close()
                currSortMap["sortColumn"] = sortCol
                currSortMap["sortDir"] = sortDir
                print "Sort Map: ", currSortMap
                print "Filter Map: ", currFilterMap
                # currvalues = [minMaxDateList[0], minMaxDateList[1], minMaxCountList[0], minMaxCountList[1]]
                return render_template('DBdisplay.html', currFilterMap=currFilterMap, minmaxdatetuple=minMaxDateList,
                                       minmaxCounttuple=minMaxCountList, currSortMap=currSortMap,
                                       data_tuple=data, column_tuple=column, columnMapTuple=columnMap,
                                       dynamicFilterMap=dynamicFilterMap)

            elif request.form['submitbtn'] == 'Filter':
                fromDate = request.form['dateFromFilter']
                toDate = request.form['dateToFilter']
                filterVals = {}
                for key in columnMap:
                    htmlKey = key.replace(' ', '_') + "[]"
                    print htmlKey
                    if htmlKey in request.form:
                        print htmlKey
                        if key in filterVals:
                            print "==================Should not be called=================="
                            filterVals[key].append(request.form[htmlKey])
                        else:
                            filterVals[key] = request.form.getlist(htmlKey)
                print fromDate
                print toDate
                print filterVals
                if "Count" in semanticMap:
                    countRange = request.form['count']
                    minCount, maxCount = countRange.split(" - ")
                conn = mysql.connect()
                cursor = conn.cursor()

                # TODO: Optimize the formation of the queries
                querystr = "CREATE OR REPLACE VIEW " + viewname + " AS Select * from " + tablename + \
                           " WHERE " + dateCol + " BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                #           + "' AND " + countCol + " BETWEEN '" + minCount + "' AND '" + maxCount + "'"
                if "Count" in semanticMap:
                    querystr += " AND " + countCol + " BETWEEN '" + minCount + "' AND '" + maxCount + "'"
                    # querystr += countCol + " BETWEEN '" + minCount + "' AND '" + maxCount + "'"
                for key, values in filterVals.iteritems():
                    key = "`{0}`".format(key)
                    querystr += " AND ("
                    querystr += key + " = '" + values[0] + "'"
                    for value in values[1:]:
                        querystr += " OR " + key + " = '" + value + "'"
                    querystr += ")"

                print querystr
                tableorview[0] = viewname
                cursor.execute(querystr)
                cursor.execute("Select * from " + viewname)
                data = cursor.fetchall()
                for columns in column:
                    if "{0}".format(columns[0]) not in [dateCol, countCol]:
                        # queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tablename)
                        queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tableorview[0])
                        cursor.execute(queryString)
                        columnMap[columns[0]] = cursor.fetchall()
                conn.close()
                currFilterMap['dateFromFilter'] = fromDate
                currFilterMap['dateToFilter'] = toDate
                if "Count" in semanticMap:
                    currFilterMap['minCount'] = minCount
                    currFilterMap['maxCount'] = maxCount
                for key, value in filterVals.viewitems():
                    htmlKey = key.replace(' ', '_')  # + "[]"
                    value = [x.encode('utf-8') for x in value]    # Encoded because selectpicker was not able to select multiple unicode values
                    dynamicFilterMap[htmlKey] = value
                print "Sort Map: ", currSortMap
                print "Filter Map: ", currFilterMap
                print "Dynamic Filter Map: ",dynamicFilterMap
                return render_template('DBdisplay.html', currFilterMap=currFilterMap, minmaxdatetuple=minMaxDateList,
                                       minmaxCounttuple=minMaxCountList, currSortMap=currSortMap,
                                       data_tuple=data, column_tuple=column, columnMapTuple=columnMap,
                                       dynamicFilterMap=dynamicFilterMap)
        else:
            print "Get called"
            currSortMap.clear()
            currFilterMap.clear()
            dynamicFilterMap.clear()
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("Select * from " + tablename + " order by " + dateCol + ";")
            data = cursor.fetchall()
            tableorview[0] = tablename
            for columns in column:
                if "{0}".format(columns[0]) not in [dateCol, countCol]:
                    # queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tablename)
                    queryString = "Select distinct(`{0}`) from {1} order by `{0}`".format(columns[0], tableorview[0])
                    cursor.execute(queryString)
                    columnMap[columns[0]] = cursor.fetchall()
            conn.close()
            currFilterMap['dateFromFilter'] = minMaxDateList[0]
            currFilterMap['dateToFilter'] = minMaxDateList[1]
            if minMaxCountList:
                currFilterMap['minCount'] = minMaxCountList[0]
                currFilterMap['maxCount'] = minMaxCountList[1]
            print "Sort Map: ", currSortMap
            print "Filter Map: ", currFilterMap
            print "Dynamic Filter Map: ",dynamicFilterMap
            # print data
            # currvalues = [minMaxDateList[0], minMaxDateList[1], minMaxCountList[0], minMaxCountList[1]]
            return render_template('DBdisplay.html', currFilterMap=currFilterMap, minmaxdatetuple=minMaxDateList,
                                   minmaxCounttuple=minMaxCountList, currSortMap=currSortMap,
                                   data_tuple=data, column_tuple=column, columnMapTuple=columnMap,
                                   dynamicFilterMap=dynamicFilterMap)
    else:
        return render_template('error.html', error='Unauthorized Access')


@app.route('/checkLogin', methods=['POST'])
def checkLogin():
    try:
        _username = request.form['uName']
        _password = request.form['uPass']
        print(_username)
        print(_password)
        if _username == "saurabh" and _password == "iitkgp":
            session['user'] = _username
            return redirect('/showDB')
        elif (_username == "admin" or _username == "ADMIN") and _password == "admin123":
            session['user'] = _username
            if os.path.exists(syntaxFile) and os.path.exists(syntaxFile):
                print "File found. Redirecting to update"
                return redirect('/updateConfig')
            else:
                print "File not found. Redirecting to create"
                return redirect('/updateConfig')
        else:
            return render_template('error.html', error='Wrong Email address or Password.')
    except Exception as e:
        print(str(e))
        # return render_template('error.html', error=str(e))


@app.route('/showLogin')
def showLogin():
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')


@app.route('/getSummary', methods=['GET', 'POST'])
def getSummary():
    if session.get('user'):
        summaryHeader = ["ID", "Frequency", "Start Date", "End Date"]
        durationSqlMap = {
            "Last Month": "1 month",
            "Last 6 Months": "6 month",
            "Last Year": "1 year",
            "Overall": ""
        }
        with open(syntaxFile) as syntax_file:
            syntaxMap = ujson.load(syntax_file)

        with open(semanticFile) as semantic_file:
            semanticMap = ujson.load(semantic_file)
        conn = mysql.connect()
        cursor = conn.cursor()
        dateCol = syntaxMap[semanticMap["Latest Timestamp"]][db_column_name]
        if semanticMap["Frequency"] != "":
            countCol = syntaxMap[semanticMap["Frequency"]][db_column_name]
        summaryColumnList = []
        for key,value in syntaxMap.viewitems():
            # print value[summary_index]
            if value[summary_index] == "Y":
                summaryColumnList.append(value[db_column_name])
        # print "syntaxMap is"
        # print syntaxMap
        # print "Summary columns are"
        # print summaryColumnList
        if semanticMap["Frequency"] != "":
#        if "Frequency" in semanticMap:
        # if countCol != "":
            countFunc = "SUM(" + countCol + ")"
        else:
            countFunc = "COUNT(*)"

        cursor.execute("Select MAX(`{0}`) from {1};".format(dateCol, tablename))
        maxDate = cursor.fetchall()
        maxDatestr = maxDate[0][0]
        if request.method == "POST":
            print (request.form)
            duration = request.form['duration']
            interval = request.form['interval']
#            cursor.execute("CREATE OR REPLACE VIEW {0} AS select * from {1} where `{2}` > DATE_SUB('{3}', INTERVAL " +
#                           durationSqlMap[duration] + ");".format(summaryViewName, tablename, dateCol, maxDatestr))
            if durationSqlMap[duration] == "":
                cursor.execute("CREATE OR REPLACE VIEW {0} AS select * from {1};".format(summaryViewName, tablename))
            else:
                cursor.execute("CREATE OR REPLACE VIEW {0} AS select * from {1} where `{2}` > DATE_SUB('{3}', INTERVAL {4});".format(summaryViewName, tablename, dateCol, maxDatestr, durationSqlMap[duration]))
            summaryMap = {}
            view = summaryViewName
            cursor.execute("SELECT DISTINCT MONTH(`{0}`), YEAR(`{0}`) from {1}".format(dateCol, view))
            monthYears = cursor.fetchall()
            print monthYears
            for month,year in monthYears:
                for column in summaryColumnList:
                    queryStr = "SELECT `{3}`, {4} FROM {5} WHERE MONTH(`{0}`)={1} AND YEAR(`{0}`)={2} GROUP BY `{3}` ORDER BY {4} DESC LIMIT 5;".format(dateCol,month,year, column, countFunc, view)
                    # print queryStr
                    cursor.execute(queryStr)
                    data = cursor.fetchall()
                    data = list(data)
                    data = [list(x) for x in data]
                    # minDate, maxDate = getMinMaxDate(column, data)
                    date = 0
                    getMinMaxDate(column, data, dateCol, cursor, view, date, month, year)
                    if column in summaryMap:
                        summaryMap[column].extend([data])
                    else:
                        summaryMap[column] = [data]
                    # print data

            # for col in summaryMap:
    #        pprint(summaryMap)
            '''
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='High'")
            highRiskCount = cursor.fetchall()
            # print highRiskCount[0][0]
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='Medium'")
            medRiskCount = cursor.fetchall()
            # print medRiskCount[0][0]
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='Low'")
            lowRiskCount = cursor.fetchall()
            # print lowRiskCount[0][0]
            cursor.execute("SELECT `" + srcipColumn + "`, SUM(" + countCol + ") FROM " + tableorview[0] + " WHERE NOT `" + srcipColumn + "` = 'Multiple' GROUP BY `" + srcipColumn + "` ORDER BY SUM(" + countCol + ") DESC LIMIT 5;")
            frequentSource = cursor.fetchall()
            # print frequentSource
            cursor.execute("SELECT `" + destipColumn + "`, SUM(" + countCol + ") FROM " + tableorview[0] + " WHERE NOT `" + destipColumn + "` = 'Multiple' GROUP BY `" + destipColumn + "` ORDER BY SUM(" + countCol + ") DESC LIMIT 5;")
            frequentDestination = cursor.fetchall()
            # print frequentDestination
            cursor.execute("SELECT sum(" + countCol + "), `" + srcipColumn + "`, `" + destipColumn + "` FROM " + tableorview[0] + " WHERE NOT `" + srcipColumn + "` = 'Multiple' AND NOT `" + destipColumn + "` = 'Multiple' GROUP BY `" + srcipColumn + "`, `" + destipColumn + "` ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            frequentStream = cursor.fetchall()
            # print frequentStream
            # print (data)
            cursor.execute("SELECT YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), sum(" + countCol + ") FROM " + tableorview[0] + " GROUP BY YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`) ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            monthwise = cursor.fetchall()
            # print monthwise
            cursor.execute("SELECT YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), DAY(`" + dateCol + "`), sum(" + countCol + ") FROM " + tableorview[0] + " GROUP BY YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), DAY(`" + dateCol + "`) ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            daywise = cursor.fetchall()
            # print daywise
            '''
            conn.close()
            durationList = ["Last Month","Last 6 Months", "Last Year", "Overall"]
            intervalList = ["Daily", "Monthly", "Yearly"]
            '''
            '''
            # return "Hello"
            # return render_template('summary.html', highRiskCount=highRiskCount, medRiskCount=medRiskCount, lowRiskCount=lowRiskCount, frequentSource=frequentSource, frequentDestination=frequentDestination, summaryMap=summaryMap)
            return render_template('summary.html', summaryMap=summaryMap, summaryHeader=summaryHeader,
                                   durationList=durationList, intervalList=intervalList)
        else:
            cursor.execute("CREATE OR REPLACE VIEW {0} AS select * from {1} where `{2}` > DATE_SUB('{3}', INTERVAL 6 MONTH);".format(summaryViewName, tablename, dateCol, maxDatestr))
            summaryMap = {}
            view = summaryViewName
            cursor.execute("SELECT DISTINCT MONTH(`{0}`), YEAR(`{0}`) from {1}".format(dateCol, view))
            monthYears = cursor.fetchall()
            print monthYears
            for month,year in monthYears:
                for column in summaryColumnList:
                    queryStr = "SELECT `{3}`, {4} FROM {5} WHERE MONTH(`{0}`)={1} AND YEAR(`{0}`)={2} GROUP BY `{3}` ORDER BY {4} DESC LIMIT 5;".format(dateCol,month,year, column, countFunc, view)
                    # print queryStr
                    cursor.execute(queryStr)
                    data = cursor.fetchall()
                    data = list(data)
                    data = [list(x) for x in data]
                    # minDate, maxDate = getMinMaxDate(column, data)
                    date = 0
                    getMinMaxDate(column, data, dateCol, cursor, view, date, month, year)
                    if column in summaryMap:
                        summaryMap[column].extend([data])
                    else:
                        summaryMap[column] = [data]
                        # print data

                        # for col in summaryMap:
                    #        pprint(summaryMap)
            '''
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='High'")
            highRiskCount = cursor.fetchall()
            # print highRiskCount[0][0]
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='Medium'")
            medRiskCount = cursor.fetchall()
            # print medRiskCount[0][0]
            cursor.execute("Select SUM(" + countCol + ") from " + tableorview[0] + " where Risk='Low'")
            lowRiskCount = cursor.fetchall()
            # print lowRiskCount[0][0]
            cursor.execute("SELECT `" + srcipColumn + "`, SUM(" + countCol + ") FROM " + tableorview[0] + " WHERE NOT `" + srcipColumn + "` = 'Multiple' GROUP BY `" + srcipColumn + "` ORDER BY SUM(" + countCol + ") DESC LIMIT 5;")
            frequentSource = cursor.fetchall()
            # print frequentSource
            cursor.execute("SELECT `" + destipColumn + "`, SUM(" + countCol + ") FROM " + tableorview[0] + " WHERE NOT `" + destipColumn + "` = 'Multiple' GROUP BY `" + destipColumn + "` ORDER BY SUM(" + countCol + ") DESC LIMIT 5;")
            frequentDestination = cursor.fetchall()
            # print frequentDestination
            cursor.execute("SELECT sum(" + countCol + "), `" + srcipColumn + "`, `" + destipColumn + "` FROM " + tableorview[0] + " WHERE NOT `" + srcipColumn + "` = 'Multiple' AND NOT `" + destipColumn + "` = 'Multiple' GROUP BY `" + srcipColumn + "`, `" + destipColumn + "` ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            frequentStream = cursor.fetchall()
            # print frequentStream
            # print (data)
            cursor.execute("SELECT YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), sum(" + countCol + ") FROM " + tableorview[0] + " GROUP BY YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`) ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            monthwise = cursor.fetchall()
            # print monthwise
            cursor.execute("SELECT YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), DAY(`" + dateCol + "`), sum(" + countCol + ") FROM " + tableorview[0] + " GROUP BY YEAR(`" + dateCol + "`), MONTH(`" + dateCol + "`), DAY(`" + dateCol + "`) ORDER BY sum(" + countCol + ") DESC LIMIT 5;")
            daywise = cursor.fetchall()
            # print daywise
            '''
            conn.close()
            durationList = ["Last Month","Last 6 Months", "Last Year", "Overall"]
            intervalList = ["Daily", "Monthly", "Yearly"]
            '''
            '''
            # return "Hello"
            # return render_template('summary.html', highRiskCount=highRiskCount, medRiskCount=medRiskCount, lowRiskCount=lowRiskCount, frequentSource=frequentSource, frequentDestination=frequentDestination, summaryMap=summaryMap)
            return render_template('summary.html', summaryMap=summaryMap, summaryHeader=summaryHeader,
                                   durationList=durationList, intervalList=intervalList)
    else:
        return render_template('error.html', error='Unauthorized Access')


def getMinMaxDate(column, data, dateCol, cursor, view, date, month, year):
    for i, value in enumerate(data):
        minmaxquery = ('Select min(`{0}`), max(`{0}`) from {5} where `{1}`="{2}" and MONTH(`{0}`)={3} and YEAR(`{0}`)={4};').format(dateCol, column, value[0], month, year, view)
        cursor.execute(minmaxquery)
        temp = cursor.fetchall()
        # value = list(value)
        for val in temp:
            value.extend(val)
        data[i] = value
        # data[i]=tuple(list(data[i]).extend(temp))

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico')


@app.route('/admin')
def admin():
    return render_template('admin.html')


@app.route('/updateConfig', methods=['GET', 'POST'])
def updateConfig():
    tempColumn = ["Latest Timestamp", "Frequency"]
    logserverColumn = ["Hostname", "Username", "Password", "Path"]
    synfields = ["CSV column name", "DB column name", "Column rank", "Column Datatype", "In Summary?"]
    synIds = ["CSVCol", "DBCol", "ColRank", "ColType", "SummaryCol"]
    textSynIds = ["CSVCol", "DBCol", "ColRank"]
    selectSynIds = ["ColType"]
    checkSynIds = ["SummaryCol"]
    typesSupported = ["Number", "Text", "Timestamp", "IP Address"]
    synmatrix = []
    syntaxMap = {}
    semanticMap = {}
    logserverMap = {}
    if request.method == 'POST':
        print request.form
        print synIds
        for synid in synIds:
            htmlid = synid + "[]"
            if htmlid in request.form:
                print htmlid
                idlist = request.form.getlist(htmlid)
                print idlist
                if synid in checkSynIds:
                    newList = []
                    for i,val in enumerate(idlist[:-1]):
                        if idlist[i] == "N":
                            if idlist[i+1] == "Y":
                                # i+=1
                                newList.append("Y")
                            else:
                                newList.append("N")
                    if idlist[-1] == 'N':
                        newList.append(idlist[-1])
                    idlist = newList
                print idlist
                synmatrix.append(idlist)
        synmatrixT = numpy.asarray(synmatrix).T.tolist()
        for row in synmatrixT:
            if row[0] != '':
                syntaxMap[row[0]] = row[1:]
        for symid in tempColumn:
            htmlid = symid.replace(" ", "_") + "[]"
            if htmlid in request.form:
                print htmlid
                idval = request.form[htmlid]
                semanticMap[symid] = idval

        for id in logserverColumn:
            htmlid = id + "[]"
            if htmlid in request.form:
                print htmlid
                idval = request.form[htmlid]
                logserverMap[id] = idval

        print synmatrix
        print syntaxMap
        print semanticMap

        with open(syntaxFile, 'w') as fp:
            ujson.dump(syntaxMap, fp)
        with open(semanticFile, 'w') as fp:
            ujson.dump(semanticMap, fp)
        with open(logserverFile, 'w') as fp:
            ujson.dump(logserverMap, fp)
        execfile("parse_log.py")
        return redirect("/admin")
        pass
    else:
        syntaxTuple = []
        textSyntaxTuple = []
        selectSyntaxTuple = []
        if os.path.exists(syntaxFile) and os.path.exists(semanticFile) and os.path.exists(logserverFile):
            with open(syntaxFile, 'r') as fp:
                syntaxMap = json.load(fp, object_pairs_hook=OrderedDict)
            with open(semanticFile, 'r') as fp:
                semanticMap = json.load(fp, object_pairs_hook=OrderedDict)
            with open(logserverFile, 'r') as fp:
                logserverMap = json.load(fp, object_pairs_hook=OrderedDict)
            for key,value in syntaxMap.viewitems():
                templist = [key]
                templist.extend(value)
                syntaxTuple.append(templist)
            syntaxTuple = sorted(syntaxTuple, key=lambda x: int(x[column_order+1]))
            '''
            # print syntaxTuple
            for index,row in enumerate(syntaxTuple):
                print index
                print row
            '''
        if not syntaxTuple:
            templist = []
            for field in synfields:
                templist.append("")         # Creating empty cells in case of fresh config
            syntaxTuple.append(templist)
        print "===================================================="
        print synIds
        print syntaxTuple
        print "===================================================="
        return render_template('updateConfig.html', columnTuple=tempColumn, syntaxFields=synfields, syntaxIDs=synIds,
                               syntaxMap=syntaxMap, semanticMap=semanticMap, logserverMap=logserverMap,
                               logserverColumn=logserverColumn, dbNameIndex=db_column_name, datatype_index=datatype_index,
                               column_order=column_order, syntaxTuple=syntaxTuple, textSyntaxIDs=textSynIds,
                               selectSyntaxIDs=selectSynIds, checkSyntaxIDs=checkSynIds, supportedTypes=typesSupported)


@app.route('/printTable', methods=['GET', 'POST'])
def printTable():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT column_name FROM information_schema.columns WHERE table_name='" + tablename + "'")
    column = cursor.fetchall()
    cursor.execute("Select * from " + tableorview[0])
    data = cursor.fetchall()
    rendered_template = render_template('mainTable.html', data_tuple=data, column_tuple=column)
    print "template rendered"
#    print rendered_template
    with open('temp.html', 'w') as outfile:
        outfile.write(rendered_template)
    print "rendering pdf"
    return render_pdf(HTML(string=rendered_template))#, stylesheets='static/css/printStyle.css')
    rendered_template = rendered_template.encode('utf-8')
    with open('temp1.html', 'w') as outfile:
        outfile.write(rendered_template)
    '''
    pdf = pdfkit.from_string(rendered_template, False, 'out.pdf') #, css='./static/css/printStyle.css')
    return make_response(pdf)
    '''
#    '''
    # rendered_template = rendered_template.encode('utf-8')
    # pdf = pdfkit.from_string(rendered_template, False) # , css='./static/styles.css')
    # print rendered_template
    # strIO = StringIO.StringIO()
    # strIO.write('Hello from Dan Jacob and Stephane Wirtel !')
    print "Creating PDF"
#    pisa.showLogging()
    pdf_file = create_pdf(rendered_template)
    print "createPDF returned"
    pdf_file.seek(0)
    return send_file(pdf_file, attachment_filename="tableData.pdf", as_attachment=True)
    resp = make_response(pdf_file)
    print "make_response returned"
    resp.headers["Content-Disposition"] = "attachment; filename=tableData.pdf"
    return resp
 #   '''


def create_pdf(pdf_data):
    pdf_file = StringIO()
    pisa.CreatePDF(StringIO(pdf_data), pdf_file)
    # pisa.pisaDocument(StringIO(pdf_data), pdf_file)
    return pdf_file


@app.template_global(name='zip')
def _zip(*args, **kwargs): #to not overwrite builtin zip in globals
    return __builtins__.zip(*args, **kwargs)

@app.context_processor
def inject_enumerate():
    return dict(enumerate=enumerate)

if __name__ == "__main__":
    context = ('../Config/progtel.cert', '../Config/progtel.key')
    app.run(host='0.0.0.0', debug=True, threaded=True) #  , ssl_context=context)
    # app.run(debug=True)
