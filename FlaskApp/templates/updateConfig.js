$(document).ready(function() {

    $('.selectpicker').selectpicker({
        size: 6 ,
        width: 'fit'
    });

        $('.addCF').click(function() {
		var row = $(document.createElement('tr')).css({
            		padding: '3px'
       	});
        {% for id in textSyntaxIDs %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;" align="center"> <input type="text" name="{{id}}[]" style="width:170px;" /></td>');
		{% endfor %}
        {% for id in selectSyntaxIDs %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;" align="center"> <select name="{{id}}[]" class="selectpicker"  align="center"> {% for type in supportedTypes %} <option value="{{type}}">{{type}}</option> {% endfor %} </select>	{% endfor %}</td>');
        {% for id in checkSyntaxIDs %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;" align="center"> <input name="{{id}}[]" type="hidden" value="N"> <input name="{{id}}[]" type="checkbox" value="Y"> </td>');
		{% endfor %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;" align="center"> <a href="javascript:void(0);" class="remCF">Remove</a> </td>');
		$('#syntaxtable').append(row);
        $('.selectpicker').selectpicker({
            size: 6 ,
            width: 'fit'
        });
        });
	$("#syntaxtable").on('click','.remCF',function(){
		console.log("Remove called");
        	$(this).parent().parent().remove();
    	});
    	$("#Latest_Timestamp").prop('required',true);
});