$(document).ready(function() {
        var iCnt = 1;
        $('.addCF').click(function() {
		var row = $(document.createElement('tr')).css({
            		padding: '3px'
       	});
        iCnt = iCnt + 1;
        {% for id in syntaxIDs %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;"> <input type="text" id="{{id}}' + iCnt +  '" name="{{id}}[]" style="width:170px;" /></td>');
		{% endfor %}
		$(row).append('<td style="width: 20%; padding-top: .5em; padding-bottom: .5em;"> <a href="javascript:void(0);" class="remCF">Remove</a> </td>');
		$('#syntaxtable').append(row);
        });
	$("#syntaxtable").on('click','.remCF',function(){
		console.log("Remove called");
        	$(this).parent().parent().remove();
    	});
});