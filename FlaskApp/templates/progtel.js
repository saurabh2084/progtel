$(document).ready(function(){
    /*
    var doc = new jsPDF('p', 'pt', 'letter');
    var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
        }
    };

    $('#cmd').click(function () {
        doc.fromHTML($('#dataTable').get(0), 5, 5, {
            'width': 1570,
            'elementHandlers': specialElementHandlers
        },
        function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF
        //          this allow the insertion of new lines after html
        doc.save('sample-file.pdf');
        });
        //doc.save('sample-file.pdf');
    });
    */

    $('.filterselectpicker').selectpicker({
        size: 6 ,
        width: '100%'
    });
    $('.sortselectpicker').selectpicker({
        size: 6 ,
        width: 'fit'
    });
    //$('#display').paginate();
    /*
    $('#display').paginate({
        limit: 100,
        onSelect: function(obj, page) {
            console.log('Page ' + page + ' selected!' );
        }
    });
    */
    $('#display').paging({
    	limit: 100
	});
    {% for key, values in columnMapTuple.iteritems() %}
    $("#{{key.replace(' ','_')}}").prop("selectedIndex", -1);
    {% endfor %}
    {#
    /*
    $('#{{key.replace(' ','_')}}').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                enableFiltering:true,
                nonSelectedText :'<Select>'
    });
    */

    /*
    $('#btnSelected').click(function () {
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
    });
    */
    #}

	$("#toggleFilter").click(function(){
		var button_text = $('#toggleFilter').text();
		if (button_text == "Show Filters")
		{
			$('#toggleFilter').html("Hide Filters")
			$("#filterDiv").show();
			$("#dataTable").attr("class", "col-xs-9 col-sm-9 col-md-9 col-lg-9");
		}
		else if (button_text == "Hide Filters")
		{
			$('#toggleFilter').html("Show Filters")
			$("#filterDiv").hide();
			$("#dataTable").attr("class", "col-xs-12 col-sm-12 col-md-12 col-lg-12");
		}
	});
	$('#dateFromFilter').datepicker({
		dateFormat: 'yy-mm-dd',
		minDate: '{{ minmaxdatetuple[0] }}',
		//minDate: '2013-7-10',
          	//maxDate: '2013-10-10',
          	maxDate: '{{ minmaxdatetuple[1] }}',
		onSelect: function(selectedDate) {
			console.log("from_date selected");
			$('#dateToFilter').datepicker('option', 'minDate', selectedDate || '{{ minmaxdatetuple[0] }}');
			//$('#dateToFilter').datepicker('option', 'minDate', selectedDate || '2013-09-10');
		}
	});
	{% if minmaxCounttuple %}
	$( "#countSlider" ).slider({
               range:true,
               min: {{ minmaxCounttuple[0] }},
               max: {{ minmaxCounttuple[1] }},
               values: [ {{ minmaxCounttuple[0] }}, {{ minmaxCounttuple[1] }} ],
               slide: function( event, ui ) {
                  $( "#count" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
               }
    });
    $( "#count" ).val( $( "#countSlider" ).slider( "values", 0 ) + " - " + $( "#countSlider" ).slider( "values", 1 ) );
    {% endif %}
	$('#dateToFilter').datepicker({
		dateFormat: 'yy-mm-dd',
		//minDate: '2013-09-10',
		minDate: '{{ minmaxdatetuple[0] }}',
          	//maxDate: '2013-10-10',
          	maxDate: '{{ minmaxdatetuple[1] }}',
		onSelect: function(selectedDate) {
			console.log("to_date selected");
			$('#dateFromFilter').datepicker('option', 'maxDate', selectedDate || '{{ minmaxdatetuple[1] }}');
			//$('#dateFromFilter').datepicker('option', 'maxDate', selectedDate || '2013-10-10');
		}
	});
	$('#dateFromFilter').val("{{ currFilterMap['dateFromFilter'] }}");
	$('#dateToFilter').val("{{ currFilterMap['dateToFilter'] }}");
	{% if currFilterMap.get('minCount') and currFilterMap.get('maxCount') %}
	$("#countSlider").slider('values',0,{{ currFilterMap['minCount'] }});
	$("#countSlider").slider('values',1,{{ currFilterMap['maxCount'] }});
	{% endif %}
	$("#count").val("{{ currFilterMap['minCount'] }} - {{ currFilterMap['maxCount'] }}");
	$("#sortColumn").val("{{ currSortMap['sortColumn'] }}");
	$("#sortDir").val("{{ currSortMap['sortDir'] }}");
    {% for key, valueList in dynamicFilterMap.viewitems() %}
        $("#{{key}}").val({{ valueList }});
    {% endfor %}
    $('.filterselectpicker').selectpicker('render');
    $('.sortselectpicker').selectpicker('render');
	$("#toggleFilter").click();
});