#!/usr/bin/python
import csv
import MySQLdb
import datetime
import numpy
import ujson
from netaddr import IPAddress
from pprint import pprint
import sys
import pysftp

# ================MACROS================================
db_column_name=0
column_order=1
datatype_index=2


logfile = "oneyearlog1.csv"
# logfile = "firewallLog.csv"
syntaxFile = "../Config/Syntactic_Mapping.json"
semanticFile = "../Config/semantic_Mapping.json"
logserverFile = "../Config/logserver_Mapping.json"

dataTypeMap = {
    "Text"      : "TEXT",
    "Ip Address": "VARBINARY(4)",
    "Timestamp" : "TIMESTAMP",
    "Number"    : "INTEGER"
}

dataTypeIndexMap = {
    "intColumn": [],
    "textColumn": [],
    "dateColumn": [],
    "ipColumn": []
}

# =====================  IPS log specific code ======================================
with open(syntaxFile) as syntax_file:
    syntaxMap = ujson.load(syntax_file)

with open(semanticFile) as semantic_file:
    semanticMap = ujson.load(semantic_file)

with open(logserverFile) as logserver_file:
    logserverMap = ujson.load(logserver_file)

for key,value in syntaxMap.viewitems():
    # if value[datatype_index] == "NUMBER":
    if value[datatype_index] == "Number":
        dataTypeIndexMap["intColumn"].append(value[column_order])
    # if value[datatype_index] == "TEXT":
    if value[datatype_index] == "Text":
        dataTypeIndexMap["textColumn"].append(value[column_order])
    if value[datatype_index] == "Ip Address":
        dataTypeIndexMap["ipColumn"].append(value[column_order])
    if value[datatype_index] == "Timestamp":
        dataTypeIndexMap["dateColumn"].append(value[column_order])

pprint(dataTypeIndexMap)
# sys.exit()
dateColumn = semanticMap["Latest Timestamp"]
dateIndex = syntaxMap[dateColumn][column_order]

countIndex = ""
if "Count" in semanticMap:
    countColumn = semanticMap["Count"]
    countIndex = syntaxMap[countColumn][column_order]

# srcipColumn = semanticMap["Source IP"]
# destipColumn = semanticMap["Destination IP"]

# srcipIndex = syntaxMap[srcipColumn][column_order]
# destipIndex = syntaxMap[destipColumn][column_order]


path = logserverMap["Path"]
localpath = "180.pdf"
filename = path.split("/")[-1]
print filename
host = logserverMap["Hostname"]
password = logserverMap["Password"]
username = logserverMap["Username"]
logfile = filename
with pysftp.Connection(host, username=username, password=password) as sftp:
    sftp.get(path)

print 'Download done.'

reader = csv.reader(open(logfile, "r"))
data = []
# ===================Core parsing logic ===============================

while '=========' not in next(reader)[0]:
    continue
column_list = next(reader)
# column_list[-1] = "percent_count"
data.append(column_list)
print column_list
next(reader)
count = 0
while True:
    line = next(reader)
#    print line
    if '=========' not in line[0]:
        modified_line = [None] * len(syntaxMap)
        for key, value in syntaxMap.viewitems():
            modified_line[int(value[column_order])-1] = line[column_list.index(key)]   # Changing the order of data columns based on config
        count += 1
        data.append(modified_line)
        del line[:]
    else:
        break

# TODO: Think of something more efficient
# Code to change column header according to order defined in config
listofsyntax = []
invertedSyntaxMap = {}
for key,value in syntaxMap.viewitems():
    tuple_val = tuple(value)
    syntaxMap[key] = tuple_val
    listofsyntax.append(tuple_val)
    invertedSyntaxMap[tuple_val] = key

listofsyntax = sorted(listofsyntax, key=lambda x: int(x[column_order]))  # Sorting column header based on config

print column_list
newcolumnlist = []
for element in listofsyntax:
    newcolumnlist.append(invertedSyntaxMap[element])
sql_column_list=newcolumnlist
print sql_column_list
# print data[1:]
# ===================Core parsing logic end=============================

# data_transpose = numpy.asarray(data).T.tolist()

for i, record in enumerate(data[1:]):
    for index in dataTypeIndexMap.get("intColumn", None):
        intIdx = int(index)-1
        recCount = record[intIdx]
        record[intIdx] = recCount.replace(',', '')

    for index in dataTypeIndexMap.get("dateColumn", None):
        dateIdx = int(index)-1
        date = record[dateIdx]
        record[dateIdx] = datetime.datetime.strptime(date, '%m/%d/%Y %H:%M').strftime('%Y-%m-%d %H:%M')   # Date format specific to MySQL

    for index in dataTypeIndexMap.get("ipColumn", None):
        ipIdx = int(index) - 1
        ipstr = record[ipIdx]
        record[ipIdx] = int(IPAddress(ipstr))    # not working as there are "multiple" strings in the log
    # dstIpIdx = int(destipIndex) - 1
    # dstip = record[dstIpIdx]
    # record[dstIpIdx] = int(IPAddress(dstip))
    data[i+1] = record

# value_map = {}
# data_transpose = numpy.asarray(data).T.tolist()
# for column in data_transpose:
#     value_map[column[0]] = list(set(list(column[1:])))

print(data[1:])


tableName = "IPS_Data"
sql_column = ['`{0}` {1}'.format(syntaxMap[i][db_column_name],dataTypeMap[syntaxMap[i][datatype_index]]) for i in sql_column_list]
print sql_column
createtablequery = "create table if not exists " + tableName + " (%s);" % (", ".join(sql_column))
print createtablequery
serveraddr = "localhost"
dbuser = "root"
dbpass = "123456"
dbname = "PROGTELDB"

db = MySQLdb.connect(serveraddr, dbuser, dbpass, dbname)
cursor = db.cursor()
cursor.execute("drop table if exists " + tableName)
cursor.execute(createtablequery)

for row in data[1:]:
    params = ['%s' for item in row]
    sql = 'INSERT INTO ' + tableName + ' VALUES (%s);' % ','.join(params)
    cursor.execute(sql, row)

db.commit()
db.close()
